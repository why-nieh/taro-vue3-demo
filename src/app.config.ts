// eslint-disable-next-line @typescript-eslint/no-var-requires
const pages = require('./pages'); // 不能使用import pages from './pages/的写法

console.log(pages);
/**
 * @type{import Taro.AppConfig}
 */
const APP_CONFIG = {
  pages,
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationStyle: 'custom',
    navigationBarTitleText: 'Taro Demo',
    navigationBarTextStyle: 'black',
  },
};

export default APP_CONFIG;
