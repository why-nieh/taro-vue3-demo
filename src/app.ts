import Taro from '@tarojs/taro';
import { createApp } from 'vue';
import store from '@/store';
import './app.less';
import '@/style/taro-ui-style.scss';

const app = createApp({
  onShow(options) {
    console.log('show', options);
  },
});
app.use(store);

app.config.globalProperties.$api = Taro;
app.config.globalProperties.$getRouterParam = (name: string, defaultValue: string = '') => {
  const router = Taro.getCurrentInstance().router;
  if (router) {
    const params = router.params as Record<string, string>;
    const param = params[name];
    if (param == null) {
      return defaultValue;
    }
    return param;
  }
  return defaultValue;
};

export default app;
