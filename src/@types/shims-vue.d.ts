/* eslint-disable */

import type { DefineComponent } from 'vue';
import Taro from '@tarojs/taro';

declare module '*.vue' {
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $api: typeof Taro;
    $getRouterParam: (name: string, defaultValue: string) => string;
  }
}
