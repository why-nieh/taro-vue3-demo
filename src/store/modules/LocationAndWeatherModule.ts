import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import Taro from '@tarojs/taro';
import store from '@/store';
import { amapApi } from '@/api/amap-api';

export class CurrentLocation {
  adCode!: string;

  address!: string;

  longitude!: number;

  latitude!: number;
}

export class LiveWeatherInfo {
  adcode!: string; // '110101'

  city!: string; // '东城区'

  humidity!: string; // '49'

  province!: string; // '北京'

  reporttime!: string; // '2021-02-25 11:00:20'

  temperature!: string; // '4'

  weather!: string; // '晴'

  winddirection!: string; // '西北'

  windpower!: string; // '≤3'
}

export class ForecastWeatherInfo {
  date!: string; // '2021-02-25'

  daypower!: string; // '≤3'

  daytemp!: string; // '13'

  dayweather!: string; // '多云'

  daywind!: string; // '北'

  nightpower!: string; // '≤3'

  nighttemp!: string; // '1'

  nightweather!: string; // '多云'

  nightwind!: string; // '北'

  week!: string; // '4'
}

const DEFAULT_LOCATION = {
  adCode: '371122',
  address: '山东省日照市莒县',
  longitude: 118.88485,
  latitude: 35.589979,
};

@Module({
  dynamic: true,
  store,
  name: 'weather',
  namespaced: true,
})
export default class LocationAndWeatherModule extends VuexModule {
  location: CurrentLocation = { ...DEFAULT_LOCATION };

  liveWeather: LiveWeatherInfo = {
    adcode: '',
    city: '',
    humidity: '',
    province: '',
    reporttime: '',
    temperature: '',
    weather: '',
    winddirection: '',
    windpower: '',
  };

  forecastWeatherList: Array<ForecastWeatherInfo> = [];

  get currentLocation(): CurrentLocation {
    return this.location;
  }

  get liveWeatherInfo(): LiveWeatherInfo {
    return this.liveWeather;
  }

  get forecastWeatherInfoList(): ForecastWeatherInfo[] {
    return this.forecastWeatherList;
  }

  get adCode(): undefined | string {
    return this.location && this.location.adCode;
  }

  @Mutation
  SET_LOCATION(location: CurrentLocation) {
    this.location = location;
    console.log('load location', location);
  }

  @Mutation
  SET_LIVE_WEATHER(weatherInfo: LiveWeatherInfo) {
    this.liveWeather = weatherInfo;
    console.log('load live weather info', weatherInfo);
  }

  @Mutation
  SET_FORECAST_WEATHER_LIST(weatherInfos: ForecastWeatherInfo[]) {
    this.forecastWeatherList = weatherInfos;
    console.log('load forecast weather list', weatherInfos);
  }

  @Action
  init() {
    return this.context
      .dispatch('updateLocation')
      .then(() =>
        Promise.all([
          this.context.dispatch('updateLiveWeatherInfo'),
          this.context.dispatch('updateForecastWeatherList'),
        ]),
      );
  }

  @Action
  updateLocation(): Promise<any> {
    return Taro.getLocation({
      type: 'wgs84',
      isHighAccuracy: true,
    })
      .then((res) => {
        const { longitude, latitude } = res;
        return amapApi
          .regeo({
            longitude,
            latitude,
          })
          .then((regoInfo) => {
            const location = {
              longitude,
              latitude,
              address: regoInfo.regeocode.formatted_address,
              adCode: regoInfo.regeocode.addressComponent.adcode,
            };
            this.context.commit('SET_LOCATION', location);
            return Promise.resolve(location);
          });
      })
      .catch(() => {
        this.context.commit('SET_LOCATION', DEFAULT_LOCATION);
      });
  }

  @Action
  updateForecastWeatherList() {
    if (this.context.getters.adCode) {
      return amapApi.weatherInfo(this.context.getters.adCode, 'all').then((weatherInfo) => {
        const { forecasts } = weatherInfo;
        if (forecasts && forecasts.length > 0) {
          this.context.commit('SET_FORECAST_WEATHER_LIST', forecasts[0].casts);
          return Promise.resolve(forecasts[0].casts);
        }
        Taro.atMessage({
          type: 'error',
          message: '未查询到预报天气信息!',
        });
        return Promise.reject(new Error('未查询到预报天气信息!'));
      });
    }
    Taro.atMessage({
      type: 'warning',
      message: '未获取到位置信息!',
    });
    return Promise.reject(new Error('未获取到位置信息!'));
  }

  @Action
  updateLiveWeatherInfo() {
    if (this.context.getters.adCode) {
      return amapApi.weatherInfo(this.context.getters.adCode, 'base').then((weatherInfo) => {
        const { lives } = weatherInfo;
        if (lives && lives.length > 0) {
          this.context.commit('SET_LIVE_WEATHER', lives[0]);
          return Promise.resolve(lives[0]);
        }
        Taro.atMessage({
          type: 'error',
          message: '未查询到实时天气信息!',
        });
        return Promise.reject(new Error('未查询到实时天气信息!'));
      });
    }
    Taro.atMessage({
      type: 'error',
      message: '未获取到位置信息!',
    });
    return Promise.reject(new Error('未获取到位置信息!'));
  }
}
