import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import store from '@/store';
import Taro from '@tarojs/taro';
import { KEYS } from '@/utils/storage-kit';
import { userApi } from '@/api/user-api';
import { requestKit } from '@/utils/request-kit';

export class User {
  username!: string;

  token!: string;

  constructor() {
    this.username = '';
    this.token = '';
  }
}

@Module({
  dynamic: true,
  store,
  name: 'user',
  namespaced: true,
})
export default class UserModule extends VuexModule {
  userInfo: User | null = (() => {
    const user = Taro.getStorageSync(KEYS.USER.INFO) || null;
    if (user != null) {
      requestKit.userToken = user.token;
    }
    return user;
  })();

  get userToken(): string | null {
    return (this.userInfo && this.userInfo.token) || null;
  }

  get isLogin(): boolean {
    return this.userInfo != null;
  }

  @Mutation
  SET_USER(userInfo: User) {
    this.userInfo = userInfo;
    requestKit.userToken = userInfo.token;
  }

  @Action
  login(userInfo: { username: string; password: string }) {
    return new Promise((resolve, reject) => {
      userApi
        .login(userInfo)
        .then((res) => {
          this.context.commit('SET_USER', res.user);
          resolve(res.user);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  @Action
  logout() {
    requestKit.userToken = null;
  }
}
