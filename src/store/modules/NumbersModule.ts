import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import store from '@/store';

@Module({
  dynamic: true,
  store,
  name: 'number',
  namespaced: true,
})
export default class NumbersModule extends VuexModule {
  numbers = [1, 2, 3];

  get getNumbers() {
    return this.numbers;
  }

  @Mutation
  ADD_NUMBER(num: number) {
    this.numbers.push(num);
  }

  @Action
  addNumber(num: number) {
    this.context.commit('ADD_NUMBER', num);
  }
}
