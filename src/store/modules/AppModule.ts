import { Module, VuexModule } from 'vuex-module-decorators';
import store from '@/store';

@Module({
  dynamic: true,
  store,
  name: 'app',
  namespaced: true,
})
export default class AppModule extends VuexModule {}
