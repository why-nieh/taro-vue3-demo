import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import Taro from '@tarojs/taro';
import { KEYS } from '@/utils/storage-kit';
import Line from '@/model/Line';
import LineDetail from '@/model/LineDetail';
import store from '@/store';

@Module({
  dynamic: true,
  store,
  name: 'location',
  namespaced: true,
})
export default class LocationModule extends VuexModule {
  lineList: Line[] = Taro.getStorageSync(KEYS.LINE_LIST) || [];

  get findLineByName(): (string) => Line | null {
    return (name: string): Line | null => {
      const lines = this.lineList.filter((line) => line.name === name);
      return lines && lines.length > 0 ? lines[0] : null;
    };
  }

  get findLineByTime(): (long) => Line | null {
    return (time: number): Line | null => {
      const lines = this.lineList.filter((line) => line.time === time);
      return lines && lines.length > 0 ? lines[0] : null;
    };
  }

  get findLineDetailByTime(): (long) => LineDetail | null {
    return (time: number): LineDetail | null => {
      const value = Taro.getStorageSync(`${KEYS.LOCATION_DETAIL_PREFIX}${time}`);
      return value || null;
    };
  }

  @Mutation
  ADD_LINE(name) {
    console.log('add');
    const time = Date.now();
    this.lineList.push(new Line(name, time));
    Taro.setStorageSync(KEYS.LINE_LIST, this.lineList);
    Taro.setStorageSync(`${KEYS.LOCATION_DETAIL_PREFIX}${time}`, new LineDetail(name, time));
  }

  @Mutation
  DEL_LINE({ name, time }: Line) {
    this.lineList = this.lineList.filter((l) => l.name !== name);
    Taro.setStorageSync(KEYS.LINE_LIST, this.lineList);
    Taro.removeStorageSync(`${KEYS.LOCATION_DETAIL_PREFIX}${time}`);
  }

  @Mutation
  SAVE_LOCATION_DETAIL(lineDetail: LineDetail) {
    Taro.setStorageSync(`${KEYS.LOCATION_DETAIL_PREFIX}${lineDetail.time}`, lineDetail);
  }

  @Mutation
  DEL_LOCATION_DETAIL(lineDetail: LineDetail) {
    Taro.removeStorageSync(`${KEYS.LOCATION_DETAIL_PREFIX}${lineDetail.time}`);
  }

  @Action
  addLine(name: string): Promise<any> {
    this.context.commit('ADD_LINE', name);
    return Promise.resolve();
  }

  @Action
  delLine(line: Line): Promise<any> {
    this.context.commit('DEL_LINE', line);
    return Promise.resolve();
  }

  @Action
  saveLocationDetail(lineDetail: LineDetail): Promise<any> {
    this.context.commit('SAVE_LOCATION_DETAIL', lineDetail);
    return Promise.resolve();
  }
}
