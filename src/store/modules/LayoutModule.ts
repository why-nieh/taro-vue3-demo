import { Module, Mutation, VuexModule } from 'vuex-module-decorators';
import store from '@/store';
import Taro from '@tarojs/taro';

const DEFAULT_NAV_BAR_HEIGHT = 42;

@Module({
  dynamic: true,
  store,
  name: 'layout',
  namespaced: true,
})
export default class LayoutModule extends VuexModule {
  $topStatusHeight = 0;
  $bottomMenuBarHeight = 0;
  $ready = false;
  $navBarHeight = DEFAULT_NAV_BAR_HEIGHT;

  get statusBarHeight(): number {
    return this.$topStatusHeight;
  }

  get marginTopStyle(): object {
    return { 'margin-top': `${this.$topStatusHeight}px` };
  }

  get topStyle(): object {
    return { height: `${this.$topStatusHeight}px` };
  }

  get bottomStyle(): object {
    return { height: `${this.$bottomMenuBarHeight}px` };
  }

  get ready(): boolean {
    return this.$ready;
  }

  get navBarHeight(): number {
    return this.$navBarHeight;
  }

  @Mutation
  INIT() {
    if (!this.$ready) {
      const systemInfo = Taro.getSystemInfoSync();
      this.$topStatusHeight = systemInfo.statusBarHeight;
      this.$bottomMenuBarHeight = systemInfo.screenHeight - systemInfo.safeArea.bottom;
      if (['windows'].includes(systemInfo.platform)) {
        this.$topStatusHeight = 0;
      }
      let navBarHeight = DEFAULT_NAV_BAR_HEIGHT;
      const menuButtonInfo = Taro.getMenuButtonBoundingClientRect();
      navBarHeight = menuButtonInfo.height + 2 * (menuButtonInfo.top - systemInfo.statusBarHeight);
      // navBarHeight = navBarHeight < DEFAULT_NAV_BAR_HEIGHT ? DEFAULT_NAV_BAR_HEIGHT : navBarHeight;
      this.$navBarHeight = navBarHeight;
      this.$ready = true;
    }
  }
}
