import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import store from '@/store';
import { lolApi } from '@/api/lol-api';
import { doError } from '@/utils/error-kit';
import { HeroSummary } from './YhMobaModule';

export class LolHeroSummary {
  heroId: string = ''; // "33",

  name: string = ''; // "披甲龙龟",

  alias: string = ''; // "Rammus",

  title: string = ''; // "拉莫斯",

  roles: string[] = []; // "tank","fighter"

  isWeekFree: string = ''; // "0",

  attack: string = ''; // "4",

  defense: string = ''; // "10",

  magic: string = ''; // "5",

  difficulty: string = ''; // "5",

  selectAudio: string = ''; // "https:\/\/game.gtimg.cn\/images\/lol\/act\/img\/vo\/choose\/33.ogg",

  banAudio: string = ''; //  "https:\/\/game.gtimg.cn\/images\/lol\/act\/img\/vo\/ban\/33.ogg",

  isARAMweekfree: string = ''; //  "0",

  ispermanentweekfree: string = ''; //  "0",

  changeLabel: string = ''; //  "改动英雄",

  goldPrice: string = ''; //  "3150",

  couponPrice: string = ''; //  "2500",

  camp: string = ''; // "",

  campId: string = ''; // "",

  keywords: string = ''; // "披甲龙龟,拉莫斯,龙龟,lg,pjlg,lms,Rammus,pijialonggui,lamosi,longgui"
}

export class Equipment {
  itemId!: string; // "1018",

  name!: string; // "灵巧披风",

  iconPath!: string; // "https:\/\/game.gtimg.cn\/images\/lol\/act\/img\/item\/1018.png",

  price!: string; // "600",

  description!: string; // "<mainText><stats><attention>15%<\/attention>暴击几率<\/stats><\/mainText><br>",

  maps!: string[]; // = ["瓦洛兰城市乐园", "水晶之痕", "嚎哭深渊", "", "未知", "次级架构43", "召唤师峡谷", "扭曲丛林"],

  plaintext!: string; // "提升暴击几率",

  sell!: string; // "420",

  total!: string; // "600",

  into!: string[]; // ["3031", "6676"],

  from!: string; // "",

  suitHeroId!: string; // "",

  tag!: string; //  "",

  types!: string[]; // [   "CriticalStrike" ],

  keywords!: string; // ""
}

export function getHeroIcon(alias: string): string {
  return `https://game.gtimg.cn/images/lol/act/img/champion/${alias}.png`;
}

export const LOL_HERO_TYPE = {
  tank: '坦克',
  fighter: '战士',
  assassin: '刺客',
  marksman: '射手',
  mage: '法师',
  support: '辅助',
};

@Module({
  dynamic: true,
  store,
  name: 'lol',
  namespaced: true,
})
export default class LolModule extends VuexModule {
  $heroList: Array<LolHeroSummary> = [];

  $heroVersion = '';

  $heroPublishTime = '';

  get heroVersion(): string {
    return this.$heroVersion;
  }

  // group by
  get heroListGroupByTypeMap(): { [key: number]: HeroSummary[] } {
    return this.$heroList.reduce((map, hero) => {
      hero.roles.forEach((type) => {
        let list = map[type];
        if (!list) {
          list = [];
          Object.assign(map, { [type]: list });
        }
        list.push(hero);
      });
      return map;
    }, {});
  }

  get heroPublishTime(): string {
    return this.$heroPublishTime;
  }

  get allHeroList(): Array<LolHeroSummary> {
    return this.$heroList;
  }

  get freeHeroList(): Array<LolHeroSummary> {
    console.log(new Set([...this.$heroList.map(it=>it.isWeekFree)]))
    return this.$heroList.filter((it) => it.isWeekFree !== '0');
  }

  @Mutation
  SET_HERO_LIST(heroList: Array<LolHeroSummary>) {
    this.$heroList = heroList;
  }

  @Mutation
  SET_HERO_VERSION(version: string) {
    this.$heroVersion = version;
  }

  @Mutation
  SET_HERO_PUBLISH_TIME(time: string) {
    this.$heroPublishTime = time;
  }

  @Action
  updateHeroList(): Promise<any> {
    return lolApi
      .fetchHeroList()
      .then((res) => {
        this.context.commit('SET_HERO_LIST', res.hero);
        this.context.commit('SET_HERO_VERSION', res.version);
        this.context.commit('SET_HERO_PUBLISH_TIME', res.fileTime);
        return Promise.resolve(this.$heroList);
      })
      .catch(doError);
  }

  $equipmentList: Array<Equipment> = [];

  $equipmentVersion = '';

  $equipmentPublishTime = '';

  get allEquipmentList(): Array<Equipment> {
    return this.$equipmentList;
  }

  get equipmentMap(): { [key: string]: Equipment } {
    return this.allEquipmentList.reduce((preval: { [key: string]: Equipment }, it) => {
      Object.assign(preval, { [it.itemId]: it });
      return preval;
    }, {});
  }

  get equipmentListGroupByTypeMap(): { [key: string]: Array<Equipment> } {
    const map = this.allEquipmentList.reduce((preVal: { [key: string]: Equipment[] }, it) => {
      const { types } = it;
      let l;
      if (types.length === 0) {
        l = preVal.other;
        if (!l) {
          l = [];
          Object.assign(preVal, { other: l });
        }
        l.push(it);
        return preVal;
      }
      types.forEach((t) => {
        l = preVal[t];
        if (!l) {
          l = [];
          Object.assign(preVal, { [t]: l });
        }
        l.push(it);
      });
      return preVal;
    }, {});
    Object.keys(map).forEach((key) => {
      map[key].sort((o1, o2) => +o1.total - +o2.total);
    });
    return map;
  }

  @Mutation
  SET_EQUIPMENT_LIST(equipmentList: Array<Equipment>) {
    this.$equipmentList = equipmentList;
  }

  @Mutation
  SET_EQUIPMENT_VERSION(version: string) {
    this.$equipmentVersion = version;
  }

  @Mutation
  SET_EQUIPMENT_PUBLISH_TIME(time: string) {
    this.$equipmentPublishTime = time;
  }

  @Action
  updateEquipmentList(): Promise<any> {
    return lolApi.fetchEquipment().then((res) => {
      this.context.commit(
        'SET_EQUIPMENT_LIST',
        res.items.filter((it) => it.maps.includes('召唤师峡谷')),
      );
      this.context.commit('SET_EQUIPMENT_VERSION', res.version);
      this.context.commit('SET_EQUIPMENT_PUBLISH_TIME', res.fileTime);
      return Promise.resolve();
    });
  }
}
