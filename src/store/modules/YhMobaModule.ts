import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import Taro from '@tarojs/taro';
import store from '@/store';
import { listGroupBy } from '@/utils/list-kit';
import { yhMobaApi } from '@/api/yh-moba-api';
import { doError } from '@/utils/error-kit';
import { YH_MOBA_URL } from '@/utils/yh-moba-kit';
import { getStorageSyncWithDefault, KEYS } from '@/utils/storage-kit';

export class HeroSummary {
  id!: number;

  type!: number;

  icon!: string;

  name!: string;
}

export const HERO_TYPE = {
  1: '坦克',
  2: '战士',
  3: '刺客',
  4: '射手',
  5: '法师',
  6: '辅助',
};

@Module({
  dynamic: true,
  store,
  name: 'yh-moba',
  namespaced: true,
})
export default class YhMobaModule extends VuexModule {
  $weekFreeHeroList: Array<HeroSummary> = getStorageSyncWithDefault(KEYS.YH.MOBA.FREE, []);

  $allHeroList: Array<HeroSummary> = getStorageSyncWithDefault(KEYS.YH.MOBA.ALL, []);

  get freeList() {
    return this.$weekFreeHeroList;
  }

  get allList() {
    return this.$allHeroList;
  }

  get heroListGroupByTypeMap(): { [key: number]: HeroSummary[] } {
    const map = listGroupBy(this.$allHeroList, 'type');
    console.log('here list group by type', map);
    return map;
  }

  @Mutation
  UPDATE_FREE_HERO(weekFreeHeroList: Array<HeroSummary>) {
    this.$weekFreeHeroList = weekFreeHeroList;
    Taro.setStorage({
      key: KEYS.YH.MOBA.FREE,
      data: this.$weekFreeHeroList,
    });
  }

  @Mutation
  UPDATE_ALL_HERO_LIST(allHeroList) {
    this.$allHeroList = allHeroList;
    Taro.setStorage({
      key: KEYS.YH.MOBA.ALL,
      data: this.$allHeroList,
    });
  }

  @Action
  updateFreeHero() {
    yhMobaApi
      .fetchCurrentFreeHeroList()
      .then((res: { zmlists: { id: number; type: number; name: string }[] }) => {
        const zmList = res.zmlists.map((it) => ({ ...it, icon: `${YH_MOBA_URL}hero/data/list/img/${it.id}.jpg` }));
        console.log('fetch free hero', zmList);
        this.context.commit('UPDATE_FREE_HERO', zmList);
      })
      .catch(doError);
  }

  @Action
  updateAllHeroList() {
    yhMobaApi
      .fetchAllHeroList()
      .then((res: { herolist: HeroSummary[] }) => {
        const allHeroList = res.herolist.map((it) => ({
          ...it,
          icon: `${YH_MOBA_URL}hero/data/list/img/${it.id}.jpg`,
        }));
        console.log('fetch all hero', allHeroList);
        this.context.commit('UPDATE_ALL_HERO_LIST', allHeroList);
      })
      .catch(doError);
  }
}
