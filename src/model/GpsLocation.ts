export default class GpsLocation {
  constructor(public name: string = '', public time: number = Date.now()) {}

  direction = 0;

  equipment = '电线杆';

  numOfLine = 4;

  latitude: number | undefined;

  longitude: number | undefined;

  speed: number | undefined;

  accuracy: number | undefined;
}
