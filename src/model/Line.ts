export default class Line {
  constructor(public name: string = '', public time: number = Date.now()) {}
}
