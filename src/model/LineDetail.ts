import GpsLocation from '@/model/GpsLocation';

export default class LineDetail {
  constructor(public name: string = '', public time: number = Date.now()) {}

  locations: GpsLocation[] = [];
}
