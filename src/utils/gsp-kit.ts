/**
 * 转换弧度
 * @param d
 * @returns {number}
 */
function getRad(d) {
  const { PI } = Math;
  return (d * PI) / 180.0;
}

export class GspLocation {
  /**
   * 经度
   */
  longitude!: number;

  /**
   * 纬度
   */
  latitude!: number;

  static DEFAULT: GspLocation = { latitude: 0, longitude: 0 };
}

export function calcDistance(gl1: GspLocation, gl2: GspLocation) {
  if (gl1.longitude === gl2.longitude && gl1.latitude === gl2.latitude) {
    return 0;
  }
  const lng1 = +gl1.longitude;
  const lat1 = +gl1.latitude;
  const lng2 = +gl2.longitude;
  const lat2 = +gl2.latitude;
  const f = getRad((lat1 + lat2) / 2);
  const g = getRad((lat1 - lat2) / 2);
  const l = getRad((lng1 - lng2) / 2);
  let sg = Math.sin(g);
  let sl = Math.sin(l);
  let sf = Math.sin(f);

  const a = 6378137.0; // The Radius of eath in meter.
  const fl = 1 / 298.257;
  sg *= sg;
  sl *= sl;
  sf *= sf;
  let s = sg * (1 - sl) + (1 - sf) * sl;
  const c = (1 - sg) * (1 - sl) + sf * sl;
  const w = Math.atan(Math.sqrt(s / c));
  const r = Math.sqrt(s * c) / w;
  const d = 2 * w * a;
  const h1 = (3 * r - 1) / 2 / c;
  const h2 = (3 * r + 1) / 2 / s;
  s = d * (1 + fl * (h1 * sf * (1 - sg) - h2 * (1 - sf) * sg));
  return s.toFixed(2);
}
