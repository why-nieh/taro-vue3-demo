import Taro from '@tarojs/taro';

export function doError(error): Promise<any> {
  Taro.atMessage({
    type: 'error',
    message: error,
    duration: 1000,
  });
  return Promise.resolve();
}
