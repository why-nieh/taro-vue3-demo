import Taro from '@tarojs/taro';

export const KEYS = Object.freeze({
  LINE_LIST: 'LINE_LIST',
  LOCATION_DETAIL_PREFIX: 'LOCATION_DETAIL_',
  USER: {
    INFO: 'USER.INFO',
    LAST_PAGE: 'USER.LAST_PAGE',
  },
  COMMONS: {
    LOCATION: 'COMMONS.LOCATION',
    WEATHER: 'COMMONS.WEATHER',
  },
  YH: {
    MOBA: {
      FREE: 'YH.MOBA.FREE',
      ALL: 'YH.MOBA.ALL',
    },
  },
});

export function getStorageSyncWithDefault(key: string, defaultValue: any = '') {
  const val = Taro.getStorageSync(key);
  return val === '' ? defaultValue : val;
}
