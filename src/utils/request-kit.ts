import CONFIG from '@/config/index';
import HttpRequest from './HttpRequest';

class RequestKit extends HttpRequest {
  userToken: string | null = null;

  protected getBaseUrl(): string {
    return CONFIG.REQUEST_BASE_URL;
  }

  protected getDefaultHeader(): { [p: string]: any } {
    return { 'USER-TOKEN': this.userToken };
  }

  protected checkResponse(response): boolean {
    return response.code === '200';
  }

  protected buildErrorMessage(response): string {
    return `${response.code}:${response.message}`;
  }

  protected filterResponse(response): any {
    return response.data;
  }
}

export const requestKit = new RequestKit();
