export function listGroupBy<T>(list: T[], key: string) {
  return list.reduce((map, it) => {
    let result = map;
    const k = it[key];
    let l = map[k];
    if (!l) {
      l = [];
      result = { ...map, [k]: l };
    }
    l.push(it);
    return result;
  }, {});
}
