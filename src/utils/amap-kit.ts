import CONFIG from '@/config';
import HttpRequest from './HttpRequest';

const BASE_URL = 'https://restapi.amap.com/v3';

/**
 * 高德地图web服务
 */
class AmapKit extends HttpRequest {
  protected getBaseUrl(): string {
    return BASE_URL;
  }

  protected getDefaultParamData(): { [p: string]: any } {
    return { key: CONFIG.AMAP_KEY };
  }

  protected checkResponse(response): boolean {
    return response.status === '1';
  }

  protected buildErrorMessage(response): string {
    return `${response.status}:${response.info}`;
  }
}

export const amapKit = new AmapKit();
