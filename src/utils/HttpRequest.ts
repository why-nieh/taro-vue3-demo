import Taro from '@tarojs/taro';
import CONFIG from '@/config';

export const DEFAULT_REQUEST_OPTION = Object.freeze({
  url: 'url not set',
  method: 'GET',
  dataType: 'json',
  responseType: 'text',
  timeout: CONFIG.REQUEST_TIMEOUT,
});

export default abstract class HttpRequest {
  constructor() {
    this.init();
  }

  protected init(): void {}

  protected getDefaultHeader(): { [key: string]: any } {
    return {};
  }

  protected getDefaultParamData(): { [key: string]: any } {
    return {};
  }

  protected getBaseUrl(): string {
    return '';
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  protected checkResponse(response): boolean {
    console.log('checkResponse', response);
    return true;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  protected buildErrorMessage(response): string {
    console.log('buildErrorMessage', response);
    return '网络请求异常!';
  }

  protected filterResponse(response): any {
    return response;
  }

  protected defaultCatchError(error) {
    const message = error || '网络请求异常!';
    Taro.atMessage({ type: 'error', message: message, duration: 1000 });
  }

  request(options) {
    const fullOptions = { ...DEFAULT_REQUEST_OPTION, ...options };
    if (!fullOptions.url.startsWith('http')) {
      let { url } = fullOptions;
      if (url.startsWith('/')) {
        url = url.substring(1);
      }
      let baseUrl = this.getBaseUrl();
      if (!baseUrl.endsWith('/')) {
        baseUrl += '/';
      }
      fullOptions.url = `${baseUrl}${url}`;
    }

    let header = { ...this.getDefaultHeader() };
    if (options.header) {
      header = Object.assign(header, options.header);
    }
    fullOptions.header = header;

    let paramData = { ...this.getDefaultParamData() };
    if (options.data) {
      paramData = Object.assign(paramData, options.data);
    }
    fullOptions.data = paramData;

    return Taro.request(fullOptions).then((res) => {
      const { statusCode, data } = res;
      if (statusCode !== 200) {
        return Promise.reject(new Error(`${statusCode}:资源异常!`));
      }
      if (!this.checkResponse(data)) {
        return Promise.reject(new Error(this.buildErrorMessage(data)));
      }
      return Promise.resolve(this.filterResponse(data));
    });
  }

  get(options) {
    return this.request({ method: 'GET', ...options });
  }

  post(options) {
    return this.request({ method: 'POST', ...options });
  }
}
