import HttpRequest from './HttpRequest';

export const YH_MOBA_URL = 'https://moba.yh.cn/moba/v2/pc/';

/**
 * 时空召唤数据抓取
 */
class YhMobaKit extends HttpRequest {
  protected getBaseUrl(): string {
    return YH_MOBA_URL;
  }

  protected checkResponse(response): boolean {
    return response.errcode === '0';
  }
}

export const yhMobaKit = new YhMobaKit();
