import { amapKit } from '@/utils/amap-kit';

class AmapApi {
  // see : https://developer.amap.com/api/webservice/guide/api/georegeo
  regeo(location: { longitude: number; latitude: number }, extensions: 'base' | 'all' = 'base') {
    return amapKit.get({
      url: 'geocode/regeo',
      data: {
        location: `${location.longitude},${location.latitude}`,
        extensions,
        radius: 1,
      },
    });
  }

  // see: https://developer.amap.com/api/webservice/guide/api/weatherinfo
  weatherInfo(city: string, extensions: 'base' | 'all' = 'all') {
    return amapKit.get({
      url: 'weather/weatherInfo',
      data: {
        city,
        extensions,
      },
    });
  }
}

export const amapApi = new AmapApi();
