import { lolKit } from '@/utils/lol-kit';

class LolApi {
  fetchHeroList() {
    return lolKit.get({
      url: 'https://game.gtimg.cn/images/lol/act/img/js/heroList/hero_list.js',
    });
  }

  fetchHeroDetail(heroId: string | number) {
    if (Object.is(NaN, +heroId)) {
      return Promise.reject(new Error('Hero id error'));
    }
    return lolKit
      .get({
        url: `https://game.gtimg.cn/images/lol/act/img/js/hero/${heroId}.js`,
      })
      .then((res) => res); // https://game.gtimg.cn/images/lol/act/img/champion/Annie.png
  }

  fetchEquipment() {
    return lolKit.get({
      url: 'https://game.gtimg.cn/images/lol/act/img/js/items/items.js',
    });
  }

  fetchRunes() {
    return lolKit.get({
      url: 'https://lol.qq.com/act/a20170926preseason/data/cn/runes.json',
    });
  }

  fetchSummoner() {
    return lolKit
      .get({
        url: 'https://lol.qq.com/biz/hero/summoner.js',
        dataType: 'text',
      })
      .then((res) => {
        const text = res.substring(res.indexOf('{'), res.lastIndexOf('}') + 1);
        return JSON.parse(text);
      });
  }
}

export const lolApi = new LolApi();
