import { yhMobaKit } from '@/utils/yh-moba-kit';

class YhMobaApi {
  fetchCurrentFreeHeroList() {
    return yhMobaKit.get({
      url: 'data/subpagezm.json',
    });
  }

  fetchAllHeroList() {
    return yhMobaKit.get({
      url: 'hero/data/list/list.json',
    });
  }

  fetchHeroDetail(heroId: number) {
    return yhMobaKit.get({
      url: `hero/data/${heroId}/content.json`,
    });
  }
}

export const yhMobaApi = new YhMobaApi();
