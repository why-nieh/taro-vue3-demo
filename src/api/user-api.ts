import { requestKit } from '@/utils/request-kit';

class UserApi {
  login(userInfo: { username: string; password: string }) {
    return requestKit.post({
      url: '/user/login',
      data: userInfo,
    });
  }
}

export const userApi = new UserApi();
