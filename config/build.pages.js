/*
 * 确保pages下的vue全是页面自动生成配置文件
 */

// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require('fs');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');
const pagesPath = path.resolve(__dirname, '../src/pages');

const pages = [];

function findPage(path) {
  if (fs.lstatSync(path).isDirectory()) {
    const files = fs.readdirSync(path);
    files.forEach((f) => {
      findPage(`${path}/${f}`);
    });
  }
  if (fs.lstatSync(path).isFile() && path.endsWith('vue')) {
    pages.push(path.substring(path.indexOf('pages')).replace('.vue', ''));
  }
}

findPage(pagesPath);

const text = `/* eslint-disable */
const pages = ${JSON.stringify(pages)};
// 不能用 export default pages的写法，通过 config/build.pages.js自动生成，请勿修改
module.exports = pages`;
const old = fs.readFileSync(path.resolve(__dirname, '../src/pages/index.ts'), 'utf-8').replace('\n', '');
if (old !== text.replace('\n', '')) {
  fs.writeFileSync(path.resolve(__dirname, '../src/pages/index.ts'), text);
}
